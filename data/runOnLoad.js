//Request information on the stylesheet information.  This must use firefox's script communication mechanism
//and so cannot be put into the shared code.

var pageID = undefined;
function requestStylesheetUpdate() {

    self.port.emit('getHighlightStyle');
}
self.port.on('setHighlightStyle', function(style) {
    updateStyleSheet(style);
});
function performGetFor(path) {
    var p = new promise.Promise();
    self.port.once('setBaseURL', function(url){
        promise.get(url + '/' + path + '?page_id=' + pageID).then(function(error, result) {
            p.done(error, result);
        });
    });
    self.port.emit('getBaseURL');
    return p;
}

function displayTerm(term) {
    console.log("We are displaying a term!");
    self.port.emit('displayTerm', term)
}

self.port.on('setPageID', function(page) {
    "use strict";
    pageID = page;
})


addHighlighting();