Dalhousie Glossary system - Firefox extension
=============================================

###Introduction

Contains a glossary add-on for Firefox designed to work with the Dalhousie Glossary system.  Currently connects to a
server, downloads a list of words and highlights any occurrances of those words on any webpage it encounters.


###Pre-requisites

You must have the following installed and configured on your system before developing this plugin:


  - The [GruntJS](http://gruntjs.com) build system
  - [NodeJS](http://www.nodejs.org) for Grunt
  - [Firefox 21+] (http://getfirefox.com)
  - The [Firefox Addon SDK](https://addons.mozilla.org/en-US/developers/docs/sdk/latest/)


### Building and Running
The plugin cannot be packaged or run without being built first.  The following grunt targets are available:

  - **build**: Combines and compresses the javascript and css and moves it into a `deploy` folder.
    After running this target, the `deploy` folder contains a valid plugin structure which can be interacted with
    using the `cfx` command (i.e `cfx run`) to run the command.
  - **run**: Builds the source and runs the plugin.  This will result in a new firefox window opening
    with the plugin installed.  This is identical to running grunt with the previous target, then moving to the
    `deploy` directory and running cfx run.
  - **debug**: Same as **run** except that it generates source maps to use for debugging. This target requires at least Firefox 23.
  - **xpi**: Builds the source and bundles it into an `XPI` file for deployment.
  - **clean**: Removes all built and deployed files.



### Git Notes
Because this plugin shares code with the chrome plugin, we use the
[git submodule](http://git-scm.com/book/en/Git-Tools-Submodules) structure to organize the code. Essentially what this
means is that the directory `data/shared-extension` is its own git repository, which the outer
git repo tracks.

Simply doing `git pull` followed by a `git submodule update` should update all code to a working state.

However, if you want to advance the shared code to its most recent version a simple `git pull` in the
 `data\shared-extension` directory will do However, to change the version of the shared extension code pointed to by the
 outer repository, you must also `git commit` in the outer repository to save the updated version.

###Directory Structure
The directory structure of this plugin follows that suggested by the Firefox Addon SDK.  In particular:

 * _root_
    * **Gruntfile.js** The grunt build  file.
    * **ownership.note** A note about the sources of some materials included in the plugin.
    * **package.json** The required file to tell firefox and node how the plugin is structured.
    * **README.md** Contains an explanation of how to interact with the source (this file)
    * **sample_dict.js** A sample dictionary with 14000 entries
    * _data_ A directory containing all of the glossary code for the extension, except the driver
        * **runOnLoad.js** This file is run when any webpage is loaded, after the glossary code has been loaded
    * _ lib_ A directory containing **main.js**, which is a driver for the glossary code.
    * _ doc_ Contains documentation for the plugin (currently empty)
    * *development_firefox_plugin* A directory containing a very basic firefox profile with no history saving, and all the devtools required for debugging turned on
    * _misc_ A directory containing miscellaneous files for support or recall
    * _deploy_ A directory which will be created when the `build` grunt task is run.