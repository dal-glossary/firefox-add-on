module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            all: {
                files: [
                    {
                        expand: true,
                        src: ['data/shared-extension/build/*'],
                        dest: 'deploy/data/shared/',
                        flatten: true,
                        filter:  function(file) {
                            return (grunt.option('dev')? true :  !/\.map$/.test(file)) && !/\.coffee\./.test(file) ;
                        }

                    },
                    {
                        expand: true,
                        src: ['data/*.js'],
                        dest: 'deploy/data/js',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['data/*.html'],
                        dest: 'deploy/data/html',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['lib/*.js'],
                        dest: 'deploy/lib',
                        flatten: true
                    },
                    {
                        src: 'package.json',
                        dest: 'deploy/package.json'
                    },
                    {
                        src: 'chrome.manifest',
                        dest: 'deploy/chrome.manifest'
                    },
                    {
                        expand: true,
                        src: 'chrome/**',
                        dest: 'deploy/'
                    }
                ],
                options: {
                    processContent: grunt.option('no-dev')? undefined: function(content, srcPath) {
                        if(/\.map$/.test(srcPath)) {
                            var mapFile = JSON.parse(content);
                            //chop off the ../ at the beginning of the source root;
                            mapFile.sourceRoot = "../../src/shared/" + mapFile.sourceRoot.substring(10);
                            return JSON.stringify(mapFile);
                        } else {
                            return content;

                        }
                }
                }

            },
            source: //grunt.option('no-dev')? undefined :
            {
                files: [
                    {
                        expand: true,
                        cwd: 'data/shared-extension/src/',
                        src: ['**'],
                        dest: 'deploy/src/shared/'
                    }
                ]
            }
        },

        replace: {
            welcome: {
                src: ['deploy/data/shared/welcome.html'],
                overwrite: true,
                replacements: [{
                    from: 'chrome://path/to/options/dialog',
                    to: 'chrome://jid1-0yzmDwetD2Mx3g-at-jetpack/content/connection.xul'
                }]
            }
        },
        shell: {
            buildShared: grunt.option('no-build-shared') ? {command: ''} : {
                command: 'npm install && grunt ' + (grunt.option('dev')? '--dev build' : 'build'),

                options: {
                    failOnError: true,
                    stdout: true,
                    stderr: true,
                    execOptions: {
                        cwd: 'data\\shared-extension'
                    }

                }
            },

            run: {
                command: 'cfx run --binary-args="http://en.wikipedia.org/" --templatedir="../application_template"',
                options: {
                    stdout: true,
                    stderr: true,
                    execOptions: {
                        cwd: 'deploy'
                    }
                }
            },

            debug: {
                command: 'cfx run -b "C:\\Program Files (x86)\\Aurora\\firefox.exe" -p "../development_firefox_plugin"  --templatedir="../application_template" --binary-args="http://en.wikipedia.org/"',
                options: {
                    stdout: true,
                    stderr: true,
                    execOptions: {
                        cwd: 'deploy'
                    }
                }
            },


            xpi: {
                command: 'cfx xpi --templatedir="../application_template"',
                options: {
                    stdout: true,
                    stderr: true,
                    execOptions: {
                        cwd: 'deploy'
                    }
                }
            }
        },

        clean:  ['deploy', 'data/shared-extension/build']

    }
    );



    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-text-replace');


    grunt.registerTask('build', ['shell:buildShared', 'copy', 'replace']);

    grunt.registerTask('run', ['build', 'shell:run']);

    grunt.registerTask('debug', ['build', 'shell:debug']);

    grunt.registerTask('bundle', ['build', 'shell:xpi']);

    // Default task(s).
    grunt.registerTask('default', ['run']);


};