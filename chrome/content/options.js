
function validateServerURL() {
    var serverURL = document.getElementById('serverURL').value;
    //This regular expression is borrowed from the jQuery validation library found at http://jqueryvalidation.org/
    if(!/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(serverURL)) {
        document.getElementById('serverURLError').className = "status error"
    } else {
        document.getElementById('serverURLError').className = "status";
    }
}


function initPrefs() {

    //Grab the cookie services

    var ios = Components.classes["@mozilla.org/network/io-service;1"]
        .getService(Components.interfaces.nsIIOService),
        uri = ios.newURI(document.getElementById('pref_server').value, null, null),
        cookieSvc = Components.classes["@mozilla.org/cookieService;1"]
        .getService(Components.interfaces.nsICookieService),

        //Load the cookie for the glossary server
        cookie = cookieSvc.getCookieString(uri, null),


    //Get the login and logout buttons
        loginBtn = document.getElementById('loginBtn'),
        logoutBtn = document.getElementById('logoutBtn'),
        statusBox =  document.getElementById('loginStatus');

    console.log("cookie is: " + cookie);

    //Decide if we've already logged in based on whether or not we can find the cookie
    if(cookie && /GAS=\w+/.test(cookie)) {
       loginBtn.style.display = 'none';
       logoutBtn.style.display = 'inline-block';
        statusBox.textContent = "You are currently logged in"
    } else {
        loginBtn.style.display = 'inline-block';
        logoutBtn.style.display = 'none';
        statusBox.textContent = "You are currently logged out"
    }


    //Handle clicking the login button
    loginBtn.addEventListener('command', function() {
        var loginData = {},
            xhr = new XMLHttpRequest(),
            serverURL =  document.getElementById('pref_server').value;

        //Pull the login information from the page
        loginData.user = document.getElementById('username').value;
        loginData.password = document.getElementById('password').value;

        //Save all preferences as they are currently set
        document.getElementById('glossary-authentication-settings').writePreferences(false);

        //Set up a response handler to verify the user was successfully able to login
        xhr.onload = function() {
            var cookieText;
            //if we've successfully logged in:
            if(this.status === 200) {
                cookieText = this.getResponseHeader('Set-Cookie');

                //Use the firefox cookie service to save the cookie we've been sent by the server.
                cookieSvc
                    .setCookieString(ios.newURI(serverURL, null, null), null, cookieText, null);

                //Notify the user that we've logged in
                statusBox.textContent = "Successfully Logged In";
                statusBox.className = "status success";
                loginBtn.removeAttribute('disabled');
                document.getElementById('loginBtn').style.display = "none";
                document.getElementById('logoutBtn').style.display = "inline-block";
                window.arguments[0].wrappedJSObject.onLogin();


            } else {
                statusBox.textContent = "Invalid Username or Password";
                document.getElementById('loginStatus').className = "status error";
                loginBtn.removeAttribute('disabled');
            }
        }

        //Add an error handler
        xhr.addEventListener("error", function() {
            //Notify the user of login failure
            statusBox.textContent = "Unable to connect to server";
            statusBox.className = "status error";
            loginBtn.removeAttribute('disabled');
        }, false);


        //Open a connection to the server
        xhr.open("POST",serverURL + "/user/login");

        //Make sure to handle cookies coming back
        xhr.withCredentials = true;

        //Notify the user that we're about to start logging in
        statusBox.className = "status info";
        statusBox.textContent = "Logging in...";
        loginBtn.setAttribute('disabled', 'disabled');

        xhr.setRequestHeader("Content-Type", "application/json")

        xhr.setRequestHeader("Origin", "null")

        //Send the form data
        xhr.send(JSON.stringify(loginData));


    }, true);

    //Handle logging out
    logoutBtn.addEventListener('command', function() {
        var xhr = new XMLHttpRequest(),
            serverURL =  document.getElementById('pref_server').value;

        document.getElementById('glossary-authentication-settings').writePreferences(false);

        //Set up a response handler to verify the user was successfully able to logout
        xhr.onload = function() {
            var cookieText;

            //if we've successfully logged out:
            if(this.status === 200) {

                cookieText = this.getResponseHeader('Set-Cookie');

                //Use the firefox cookie service to save the cookie we've been sent by the server.
                cookieSvc
                    .setCookieString(ios.newURI(serverURL, null, null), null, cookieText, null);


                //Notify the user that we've logged in
                statusBox.textContent = "Successfully logged out";
                statusBox.className = "status success";
                logoutBtn.removeAttribute('disabled');
                document.getElementById('loginBtn').style.display = "inline-block";
                document.getElementById('logoutBtn').style.display = "none";

                window.arguments[0].wrappedJSObject.onLogout();

            } else {
                statusBox.textContent = "Server could not log us out";
                document.getElementById('loginStatus').className = "status error";
                logoutBtn.removeAttribute('disabled');
            }
        }

        //Add an error handler
        xhr.addEventListener("error", function() {
            //Notify the user of logout failure
            statusBox.textContent = "Unable to connect to server";
            statusBox.className = "status error";
            logoutBtn.removeAttribute('disabled');
        }, false);

        //Open a connection to the server
        xhr.open("POST",serverURL + "/user/logout");

        //Make sure to handle cookies coming back
        xhr.withCredentials = true;

        xhr.setRequestHeader("Origin", "null")

        //Notify the user that we're about to start logging in
        statusBox.className = "status info";
        statusBox.textContent = "Logging out...";
        logoutBtn.setAttribute('disabled', 'disabled');

        //Send the form data
        xhr.send();


    }, true);

}