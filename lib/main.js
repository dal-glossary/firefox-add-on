
var prefs = require('sdk/simple-prefs'),

// Import the page-mod API
    pageMod = require("sdk/page-mod"),

    // Import the self API
    self = require("sdk/self"),

    panel = require("sdk/panel"),

    simpleStorage = require("sdk/simple-storage"),

    tabs = require('sdk/tabs'),

    Request = require("sdk/request").Request;

//Display the welcome page, and attach functionality to open the connection settings dialog to the appropriate link
exports.main = function (options) {

    var myPageMod,

        termPanel = panel.Panel({
            contentURL: self.data.url("shared/termview.html")
        });



    function setPageFiltering() {

        function updateFilters() {
            var pages = simpleStorage.storage.pages, filters = [];
            if (!pages) {
                pages = [];
            } else {
                pages = JSON.parse(pages);
            }
            for (let i = 0; i < pages.length; i += 1) {
                if (pages[i].url) {
                    filters.push(new RegExp(pages[i].url));
                }
            }
            if (myPageMod) {
                myPageMod.destroy();

            }
            myPageMod = createPageMod(filters);
        }

        if (!myPageMod) {
            updateFilters();
        }

        Request({
            url: prefs.prefs['serverURL'] + "/page",
            onComplete: function (response) {
                if (response.status === 200) {
                    simpleStorage.storage.pages = response.text;
                    updateFilters();
                } else if (response.status === 403) {
                    console.log("Glossary server forbid access to list of active pages.  Log in to the server to download filtering criteria");
                } else {
                    console.log("Glossary server could not create a list of pages.  This is likely due to a configuration error on the server");
                }
            }
        }).get();


    }

    function openConnectionSettings() {
        const { Cc, Ci } = require('chrome');
        const windowWatcher = Cc['@mozilla.org/embedcomp/window-watcher;1'].
            getService(Ci.nsIWindowWatcher);
        var callbackObj = {
            onLogin: function () {
                setPageFiltering();
            }
            ,
            onLogout: function () {
                myPageMod.destroy();
                myPageMod = undefined;
                simpleStorage.storage.pages = undefined;
                setPageFiltering();
            }
        };

        callbackObj.wrappedJSObject = callbackObj;

        windowWatcher.openWindow(null, 'chrome://jid1-0yzmDwetD2Mx3g-at-jetpack/content/connection.xul', null, "chrome=true", callbackObj);

//        openDialog({url: 'chrome://jid1-0yzmDwetD2Mx3g-at-jetpack/content/connection.xul',
//            features: "chrome=true,centerscreen=true,toolbar=true",
//            args:
//        });
    }

    if (options.loadReason === 'install') {
        tabs.open(
            {
                url: self.data.url('shared/welcome.html'),
                onReady: function (tab) {
                    //Look for any elements that are pointing to the options dialog, and attach the callback to it.

                    var worker = tab.attach({
                        contentScript: "let links = document.querySelectorAll('[href=\"chrome://jid1-0yzmDwetD2Mx3g-at-jetpack/content/connection.xul\"]');" +
                            "for(let item of links){item.addEventListener('click', function() {self.port.emit('openLoginDialog')})}"
                    });

                    worker.port.on('openLoginDialog', function () {
                        openConnectionSettings();
                    });
                }
        });

    }

    prefs.on("connectionSettings", openConnectionSettings);


    function createPageMod(filters) {

        if (filters.length === 0) {
            return undefined;
        }
        // Create a page mod
        return pageMod.PageMod({
            include: filters,
            contentScriptFile: [self.data.url("shared/highlight.lib.js"), self.data.url("shared/highlight.min.js"), self.data.url("js/runOnLoad.js")],
            onAttach: function (worker) {
                console.log("Attached to page");
                function sendHighlightStyle() {
                    var style = {
                        bold: prefs.prefs['highlightBold'] ? 'bold' : 'normal',
                        underline: prefs.prefs['highlightUnderline'] ? 'underline' : 'none',
                        colorenabled: prefs.prefs['highlightColorEnabled'],
                        color: prefs.prefs['highlightColor']
                    };
                    worker.port.emit("setHighlightStyle", style);
                }

                worker.port.on("getHighlightStyle", sendHighlightStyle);
                worker.port.on("getBaseURL", function () {
                    worker.port.emit("setBaseURL", prefs.prefs['serverURL']);
                });

                worker.port.on("displayTerm", function (term) {
                    termPanel.contentURL = self.data.url("shared/termview.html") + "#/term/" + term;
                    termPanel.show();
                });


                prefs.on("highlightBold", sendHighlightStyle);
                prefs.on("highlightUnderline", sendHighlightStyle);
                prefs.on("highlightColorEnabled", sendHighlightStyle);
                prefs.on("highlightColor", sendHighlightStyle);

                //Find out what page we're on, and set the page ID for the script
                //The pages array should parallel what's in the filters array.
                var pages = simpleStorage.storage.pages || "[]";
                pages = JSON.parse(pages);
                for(var index = 0; index < filters.length; index += 1) {
                    if(worker.url.match(filters[index])) {
                        worker.port.emit("setPageID", pages[index].id);
                        break;
                    }
                }

            }
        });
    }

    setPageFiltering();
};




